
<!DOCTYPE html>

<html>
<head>
	<title></title>
</head>
<?php
include "include.php";

$query = "SELECT * FROM admin where setting = 'wallpaper' ";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while ($row = mysqli_fetch_array($result)) {
	$wallpaper = $row[2];
	}
$query = "SELECT * FROM codes WHERE code='maintenance'";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
 while ($row = mysqli_fetch_array($result)) 
 {
 	 	$maintenance_mode=$row[2];

 	};

 	$query = "SELECT * FROM codes WHERE code='thermtype'";
	$result = mysqli_query($con,$query) or die (mysqli_error($con));
 	while ($row = mysqli_fetch_array($result)) 
 			{
 	 	$thermclassvalue=$row['state'];
 	 	if ($thermclassvalue=='1'){$thermclassvalue="Celcius";} else {$thermclassvalue="Fahrenheit";};

 			};



 $query='SELECT * FROM admin WHERE setting like "%therm%" ORDER BY id ASC;';
 $result = mysqli_query($con,$query) or die (mysqli_error($con));
 // print '<br><bR><br><br><br><br>';
 $key = 0;
 while ($row = mysqli_fetch_array($result)){
 		$therm="therm" . $key++;
 		$$therm = $row[2];
 		// print $therm;

 };

 $query="SELECT * FROM codes WHERE code='relaypolarity'";
 $result = mysqli_query($con,$query) or die (mysqli_error($con));
 while ($row = mysqli_fetch_array($result)){
 	$relaypolarity = $row[2];
 };

 
 ?>


<body>

<div align="center">

<div class="inline-table">
	<div class="title-block customfontsml white title-block-size">
		Manual Control Mode
	</div>
	<div class="block-body block-body-size">
	<p>Manual mode temporarlily disables your schedule giving your manual control over your power devices.</p>
		<br><br>
		<form action="admin-submit.php" method="post">			
			<input name="option" value="maintenance" hidden>
			<select class="form-control" name="maintenancemode">
				<option>Currently <?php print $maintenance_mode;?></option>
				<option value="on">Set to on</option>
				<option value="off">Set to off</optoin>
			</select>
			<button class="btn btn-default" type="submit">SET</button>
		</form>
	
	</div>
</div>


<div class="inline-table">
	<div class="title-block customfontsml white title-block-size">
		Temperature Format
	</div>
	<div class="block-body block-body-size">
	<p>Set global temperature type.</p>
	<br><Br><br><br>
		<form action="admin-submit.php" method="post">
			<input name="option" value="thermclass" hidden>
			<select class="form-control" name="thermclassvalue">
			<option>Currently <?php print $thermclassvalue;?></option>
			<option value="1">Set to Celcius</option>
			<option value="0">Set to Fahrenheit</option>
			</select>
			<button class="btn btn-default" type="submit">SET</button><?php print "Active = ".$thermclass; ?> 
		</form>	
	</div>
</div>


<div class="inline-table">
	<div class="title-block customfontsml white title-block-size">
		Temperature Sensors
	</div>
	<div class="block-body block-body-size">

	<form action="admin-submit.php" method="post">
		<input name="option" value="thermserials" hidden>
		<p>Register your temperature sensor serial numbers. Ensure you register in the right order. To test place one in hot water and check temperature on the home page.</p>
		<p>
			<div class="form-inline">Sensor 1:&nbsp&nbsp<input name="therm" class="form-control" value="<?php print $therm0;?>" placeholder="Serial Number Water"></div>
		</p>
		<p>
			<div class="form-inline">Sensor 2:&nbsp&nbsp<input name= "thermii" class="form-control" value="<?php print $therm1;?>" placeholder="Serial Number Air"></div>
		</p>
		<button class="btn btn-default" type="submit">SET</button>
	</form>
	</div>
</div>


<div class="inline-table">
	<div class="title-block customfontsml white title-block-size">
		Relay Polarity
	</div>
	<div class="block-body block-body-size">
		<form action="admin-submit.php" method="post">
			<input name="option" value="relaypolarity" hidden>
				<p>Some relays are ON by default or OFF by default, this setting allows you to set that correctly. When the Pi is powered off your devices SHOULD switch off.</p>
				<p>High = 1, Low = 0</p>
			<!-- <div class="form-inline"> -->
				<!-- <input name="relaypolarity" class="form-control" value="<?php print $relaypolarity;?>" placeholder="Polarity 1 or 0" size="13px"> -->
				<select name="relaypolarity" class="form-control">
					<option value="<?php print $relaypolarity;?>" hidden>Currently <?php print $relaypolarity;?></option>
					<option value="0">0</option>
					<option value="1">1</option>
				</select>
			<!-- </div> -->
			<button class="btn btn-default" type="submit">SET</button>
		</form>
	</div>
</div>


<div class="inline-table">
	<div class="title-block customfontsml white title-block-size">
		Admin Password
	</div>
	<div class="block-body block-body-size">
	<p>Login password.</p>
	<br><br><br><br>
		<p><form action="admin-submit.php" method="post">
			<input name="option" value="setpasscode" hidden>
			<div class="form-inline">Password: &nbsp<input name="passcode" class="form-control" value="<?php print $passcode;?>" placeholder="Enter Passcode" size="13px"></div>
			<button class="btn btn-default" type="submit">SET</button>
		</form></p>
	</div>
</div>


<div class="inline-table">
	<div class="title-block customfontsml white title-block-size">
		Water Cooling
	</div>
	<div class="block-body block-body-size">
	<p>If the water temperature threshold is exceeded Relay 8 will be activated until the temperature is reduced (fan). To disable this feature change the value to 0. Even if set in auto schedule to be off it will still switch it on. ONLY Manual will overide this control.</p>
		<form action="admin-submit.php" method="post">
				<input name="option" value="setthreshold" hidden>
				<div class="form-inline">Threshold: &nbsp<input name="thresholdvalue" class="form-control" value="<?php print $threshold;?>" placeholder="Threshold" size="13px"></div>
				
				<button class="btn btn-default" type="submit">SET</button>
		</form>
	</div>
</div>

</div>
</body>
</html>


