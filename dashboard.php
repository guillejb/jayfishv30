<?php
	include "include.php";
?>

<html>
<head>
<meta http-equiv="refresh" content="30">
</head>
<body>

<?php

// Tempereaure record count
$query="SELECT count(id) FROM therm;";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while($rows = mysqli_fetch_array($result)) {
$record_count = $rows['count(id)'];
};

// pi cpu temperature
$query="select * from therm order by id desc limit 1;";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while($rows = mysqli_fetch_array($result)) {
if ($thermtype=='1'){
$cputemp = $rows['thermiii'];
} else { $cputemp = $rows['thermiii'] *9/5+32;}

$cputemp_date = $rows['dateset'];
};

$query="SELECT count(id) FROM species;";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while($rows = mysqli_fetch_array($result)) {
$species_count = $rows['count(id)'];
};

?>
<div align="center">
<div class="<?php print $tablebackground_nolines_header;?> dashboard-width"><div class="customfont" align="center">Dashboard</div></div>
<div class="<?php print $tablebackground_nolines; ?> dashboard-width dashboard-padding">

<div align="center">
	<div class="inline tile-size1">
			<div class="tile_title tile-title-color1">
			<img src="images/water.png" width="40">
				
			</div>
				<div class="tile_body">
					<div class="customfont"><?php print $therm.$thermclass;?></div>
				</div>
			<div class="tile_footer">
				<div class="customfontsml">Water</div>
			</div>
		</div>


		<div class="inline tile-size1">
			<div class="tile_title tile-title-color1">
			<img src="images/air.png" width="50">
				
			</div>
				<div class="tile_body">
					<div class="customfont"><?php print $thermii.$thermclass;?></div>
				</div>
			<div class="tile_footer">
				<div class="customfontsml">Air</div>
			</div>
	</div>

		<div class="inline tile-size1">
			<div class="tile_title tile-title-color1">
			<img src="images/cpu.png" width="40">
				
			</div>
				<div class="tile_body">
					<div class="customfont"><?php print $cputemp.$thermclass;?></div>
				</div>
			<div class="tile_footer">
				<div class="customfontsml">Pi Cpu</div>
			</div>
		</div>


		<div class="inline tile-size1">
			<div class="tile_title tile-title-color2">
			<img src="images/fish.png" width="40">
				
			</div>
				<div class="tile_body">
					<div class="customfont"><?php print $species_count;?></div>
				</div>
			<div class="tile_footer">
				<div class="customfontsml">Species</div>
			</div>
		</div>
		


		<div class="inline tile-size1">
			<div class="tile_title tile-title-color2">
			<img src="images/clock.png" width="40">
				
			</div>
				<div class="tile_body">
					<div class="customfont"><?php print $phase;?></div>
				</div>
			<div class="tile_footer">
				<div class="customfontsml">Phase</div>
			</div>
		</div>



		<div class="inline tile-size1">
				<div class="tile_title tile-title-color2">
					<img src="images/waterchange.png" width="40">
				</div>

				<div class="tile_body">
						
						<div class="customfont">
							<?php			
							$query="SELECT DATEDIFF((SELECT dateset from event where event='waterchange' limit 1), NOW()) AS waterchangediff;";
							$result = mysqli_query($con,$query) or die (mysqli_error($con));
							while($rows = mysqli_fetch_array($result)) {
							$waterchangediff = $rows['waterchangediff'];
							};
							print $waterchangediff . " days";
							?>
						</div>
				</div>
				<div class="tile_footer">
					<div class="customfontsml">Water Change</div>
				</div>
		</div>





		<Br>
		<div class="inline tile-size2">
			<div class="tile_title tile-title-color2">
				<div class="customfont" align="center">Relays</div>
			</div>
			<div class="tile_body">
					<table class="dashboardtable">
					<tr><th>Relay</th><th>GPIO</th><th>Description</th><th>Status</th><th>Manual Setting</th></tr>
					<?php
						$query="SELECT * FROM relay WHERE gpio;";
						$result = mysqli_query($con,$query) or die (mysqli_error($con));
						while($rows = mysqli_fetch_array($result)) {
						$relay = $rows['1'];
						$gpio = $rows['2'];
						$desciption = $rows['3'];
						$manualstate = $rows['4'];
						$status = $rows['5'];
						if ($status == "checked") {$status='<div style="color:#7FFF00;">Running</div>';} else {$status='<div style="color:red;">Off</div>';};
						print "<tr><td>" . $relay ."</td><td>" .  $gpio ."</td><td>" .  $desciption ."</td><td>" .  $status . '</td><td align="center">' . $manualstate. "</td></tr>";

						};
						print '<td colspan="6">'.$threshold_warning. '</div></td>';
					?>
					</table>
			</div>
			<div class="tile_footer">
				<div class="customfontsml">Configured Relays</div>
			</div>

		 
		</div>


</div>
</div>
</body>
</html>