
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>JayFish</title>

<!-- CSS -->

<!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css">
<!-- CUSTOM CSS -->
    
    
<!-- FONT AWESOME CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
<!-- GOOGLE FONTS - LRG FONTS custom.css -->
    <link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
<!-- MENU FONT-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/custom.css">
<!-- PHP -->


<?php
session_start();
include "connection.php";

$query="SELECT * FROM admin WHERE setting='passcode';";
  $result=mysqli_query($con,$query)or die(mysqli_error($con));
  while ($rows=mysqli_fetch_array($result))
  {
     $passcode=$rows['value'];
    };

if ($_SESSION['passcode'] == $passcode) {;}else{print '<meta HTTP-EQUIV="REFRESH" content="0; url=login.php">';};


// THRESHOLD FETCH & CHECK

$query="SELECT * FROM codes WHERE code='threshold';";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while($rows = mysqli_fetch_array($result)) {
  $threshold=$rows[2];

  };

$query = "SELECT * FROM event WHERE event='tablebackground'";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while ($rows = mysqli_fetch_array($result))
    {
      $value = $rows['value'];
      global $value;
      if ($value == "Dark"){$tablefontcolor = "#FFFFFF";} else { $tablefontcolor = "#000000";};
    };
?>

<style type="text/css">
html {
    background: url(images/wallpaper.jpg) no-repeat center center fixed; 
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;

}

body { 
color: <?php print $tablefontcolor;?>;
}

.table th {border-top: 5px !important;}
</style>
</head>
<?php 

$query = "SELECT * FROM event WHERE event='tablebackground';";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while ($rows = mysqli_fetch_array($result)) {
$table_theme = $rows['value'];
};

$today = date("Y-m-d",strtotime('-0 day')); 
$yesterday = date("Y-m-d",strtotime('-1 day')); 
$tablewidth = "340";
$tablewidth_two = "1000";


if ($table_theme == "Dark") {
$tablebackground = "table dark";
$tablebackground_nolines = "plaindark";
$tablebackground_nolines_header = "plaindark_header";
$tablebackground_nolines_menubar = "plaindark";
$tablebackground_light = "table light";
$tablebackground_title = "table title"; }
else {
$tablebackground = "table tableclear";
$tablebackground_light = "table";
$tablebackground_title = "table";
$tablebackground_nolines = "tableclear";
$tablebackground_nolines_header = "tableclearheader";
$tablebackground_nolines_menubar = "menubar";
}

$query="SELECT * FROM sched";
$result=mysqli_query($con,$query)or die(mysqli_error($con));
while ($rows = mysqli_fetch_array($result))
{
  $phase =$rows[13];
};

$query="SELECT * FROM codes WHERE code='maintenance';";
$result = mysqli_query($con,$query) or die (mysqli_error($con));
while($rows = mysqli_fetch_array($result)) {
  $maintenance_state = $rows[2];
  if ($maintenance_state == "on") {$maintenance_led='<div class="mini-led-red-on"></div>';};
  // if ($maintenance_state == "on") {$maintenance_led='<div class="led-red-on"></div><font size="1">MAINTENANCE MODE ON</font>';};
  if ($maintenance_state == "off"){$maintenance_led='<div class="mini-led-on"></div>';};
   // if ($maintenance_state == "off"){$maintenance_led='<div class="led-on"></div><font size="1">SCHEDULE MODE ON</font>';};
  };

?>
<!-- TOP MENU BAR -->
<style> 
  table.menubar 
  {
        text-align: center;
        border-radius: 5px;
  } 
  
  a.menubar 
  {
    color: white;
  }
</style>
<body>

<!-- ------------------------------------------------------ MENU BAR -->

<nav class="navbar navbar-default">
  <div class="container">
     <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!-- ------------------------------------------------------ MENU BAR -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap-confirmation.js"></script>
    </body>

<nav class="nav navbar-fixed-bottom">
<?php

$query="SELECT * FROM codes WHERE code='thermtype'";
$result=mysqli_query($con,$query)or die(mysqli_error($con));
while ($rows = mysqli_fetch_array($result)) 
{
  $thermtype=$rows['state'];
};



$query="SELECT * FROM `therm` ORDER BY ID DESC LIMIT 1;";
  $result = mysqli_query($con,$query) or die (mysqli_error($con));
  while($rows = mysqli_fetch_array($result)) {
    $therm = $rows['therm'];
    $thermii = $rows['thermii'];
  };

if ($thermtype=='1')
{
$thermclass="&deg;C";
  }
else {
  $therm=$therm * 9 / 5 + 32;
  $thermii=$thermii * 9 / 5 + 32;
  $thermclass="&deg;F";
};

?>


<!-- ------------------------------------------------------ FOOTER BAR -->
<table border="1" style="width:100%;">
  <td style="background-color: white;color: black;text-align: center;">
        Water Temp:&nbsp<?php print $therm; ?>&nbsp&nbspAir:&nbsp<?php print $thermii; ?>
        &nbsp Phase:<?php print " ". $phase;?>
  </td>
</table>

</nav>
<!-- ------------------------------------------------------ FOOTER BAR -->
<?php 
if ($threshold <= $therm) {$threshold_warning='<div style="color:yellow;background-color:#8A0808;"><strong>Water Overheating Relay 8 Activated</strong></div>';} else {$threshold_warning="";};
if ($threshold == 0 ) {$threshold_warning="";};
?>

