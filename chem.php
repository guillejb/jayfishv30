<!DOCTYPE html>

<html>
<head>
	<title></title>
	<script src="js/Chart.js"></script>
</head>

<body>
<?php
include "connection.php";
include "include.php";

$query = "SELECT * FROM chem;";
$result = mysqli_query($con, $query) or die(mysqli_error($con));
print '
<div align="center">
<table class="'.$tablebackground.'" style="max-width:800px; padding:0px;">
<div class="'.$tablebackground_nolines_header.'" style="max-width:800px; padding:0px;"><div class="customfont" align="center">Chemical Readings</div></div>
<th>Chemical</th><th>Description</tH><th>Last Reading</th><th>New Reading</th><th>Date</th><th>Time</th><tr>';
print '<form action="general-submit.php" method="post">';
print '<input name="option" value="chem" hidden>';
$collect_id_chem = array();
while ($rows = mysqli_fetch_array($result))
	{
		$id = $rows['id'];
		$shortname = $rows['shortname'];
		$description= $rows['description'];
			array_push($collect_id_chem, $id);

		$query2 = "SELECT * FROM event WHERE chemid = '$id' ORDER BY id DESC LIMIT 1;";
		$result2 = mysqli_query($con,$query2) or die (mysqli_error($con));
		while ($rows2 = mysqli_fetch_array($result2))
			{
				$id2 = $rows2['chemid'];
				$dateset = $rows2['dateset'];
				$timeset = $rows2['timeset'];
				$lastvalue = $rows2['value'];
			};


		print 
		'
		<td>'.$shortname.'</td>
		<td>'.$description.'</td>
		<td>'.$lastvalue.'</td>
		<td align="left"><input class="form-control" name="form'.$id.'" style="width:100px;"></td>
		<td>'.$dateset.'</td>
		<td>'.$timeset.'</td>
		<tr>		
		';
		$lastvalue = "";
		$date = "";
		$time = "";

	};

	$collect_id_chem = implode(',', $collect_id_chem);
		
	print '
	<table class="table" style="max-width:800px;min-width:600px; padding:0px;">
		
		<td  class="'.$tablebackground_nolines_header.'" align="right">
			<button class="btn btn-default" type="submit">SUBMIT NEW VALUES</button</td>
	</table>
		<input name="chemvalues" value="'.$collect_id_chem.'" hidden>
		</form>
		</div>
	<br>';

?>

<?php 

$chemical_name_array= array();

$query3 = "SELECT shortname FROM chem ORDER BY id";
		$result3 = mysqli_query($con,$query3) or die (mysqli_error($con));
		while ($rows2 = mysqli_fetch_array($result3)) {
			$chemicalname=$rows2[0];
			// print $chemicalname;
			array_push($chemical_name_array, $chemicalname);

		};


foreach ($chemical_name_array as $key => $value) {
		// print $value . '=';
		$chemical_group = $value;
		$chemical_data_array = array();
		$chemical_data_date_array = array();
				$query2 = "SELECT value,dateset FROM event WHERE event = '$value' ORDER BY id;";
				$result2 = mysqli_query($con,$query2) or die (mysqli_error($con));
					while ($rows2 = mysqli_fetch_array($result2)) {

						$dataset = $rows2[0];
						$dataset_date = $rows2[1];
						if ($dataset == "Added") {;} else {
								array_push($chemical_data_array, $dataset);
								array_push($chemical_data_date_array, $dataset_date);
							};

						
						};

		

		// foreach ($chemical_data_array as $key => $value) {print $value;};
		$chemical_data_array_string = implode(',', $chemical_data_array);
		$chemical_data_date_array_string = implode('","', $chemical_data_date_array);
		// print '----' .$chemical_data_date_array_string. '-----';
		// print $chemical_data_array_string;
		
print '		<script>

    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData_'.$chemical_group.' = {
      labels : ["'.$chemical_data_date_array_string.'"
      ],
      datasets : [
       {
           label: "Water",
          fillColor : "rgba(100,100,205,0.2)",
          strokeColor : "rgba(151,187,205,1)",
          pointColor : "rgba(151,187,205,1)",
          pointStrokeColor : "#fff",
          pointHighlightFill : "#fff",
          pointHighlightStroke : "rgba(151,187,205,1)",
          data : ['.$chemical_data_array_string.'

          ]
        },
    
      ]

    }

</script>';

print '

<div align="center">
    <table style="padding:0px;" class="<?php print $tablebackground_nolines;?>" border="0">
    	<td style="padding-left:10px; padding-right:10px;">
       		<div align="center" class="plaindark customfont">'.$chemical_group.'</div></td><tr>
     	<td>
	        	<div style="width:100%;"><div>
	                 <canvas id="canvas_'.$chemical_group.'" height="150" width="800"></canvas>
            </div> 
            </td>
          </table> 
        <!-- </div> -->
   <!-- </div> -->
</div>

';
print '


  ';
 				# code...
};	

?>




  		

  				<?php
  			$query4 = "SELECT shortname FROM chem";
  			$chemical_name_array2 = array();
			$result4 = mysqli_query($con,$query4) or die (mysqli_error($con));
					while ($rows4 = mysqli_fetch_array($result4)) {
						$chemicalname=$rows4[0];
						// print $chemicalname;
						array_push($chemical_name_array2, $chemicalname);
						

					};
					print '<script>window.onload = function(){';
			foreach ($chemical_name_array2 as $key => $value) {
				

				print '
				
				var ctxc = document.getElementById("canvas_'.$value.'").getContext("2d");
    			window.myLine = new Chart(ctxc).Line(lineChartData_'.$value.', {responsive: false});
				
				';

				# code...
			};
			print '}</script>';


  			?> 	



   		
  




<br><br><br><br>
</body>
</html>

